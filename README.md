Some examples (appearing colored within a terminal):

$ `termitime lun-mer@10,-sab@14,-dim lun-jeu@10,-dim`

  Output:
```
                              10      14
          --------------------+-------+-------------------
   Monday ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
  Tuesday ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
Wednesday ░░░░░░░░░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
 Thursday ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
   Friday ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
 Saturday ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒░░░░░░░░░░░░░░░░░░░░
   Sunday ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░

                             [...]
```


$ `LC_ALL=es_ES.utf8 termitime -w 36 -r 8-21 l-l@12C1,-mer@12,-sab@12,-d l-j@12,-d -W -A`

  Output:
```
    8       12                21   8       12                21
    +-------+-----------------+   +-------+-----------------+
lun ▒▒▒▒▒▒▒▒░░░░░░░░░░░░░░░░░░    ░░░░░░░░░░░░░░░░░░░░░░░░░░
mar ░░░░░░░░░░░░░░░░░░░░░░░░░░    ░░░░░░░░░░░░░░░░░░░░░░░░░░
mié ░░░░░░░░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒    ░░░░░░░░░░░░░░░░░░░░░░░░░░
jue ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒    ░░░░░░░░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
vie ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒    ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
sáb ▒▒▒▒▒▒▒▒░░░░░░░░░░░░░░░░░░    ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
dom ░░░░░░░░░░░░░░░░░░░░░░░░░░    ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
```

$ `termitime -Aw 36 -r 8-21 lun-lun@12C0,-mer@12,-sab@12,-dim -c $'_,☢,✓'`

  Output:
```
     8       12                21
     +-------+-----------------+
lun. ________☢☢☢☢☢☢☢☢☢☢☢☢☢☢☢☢☢☢
mar. ☢☢☢☢☢☢☢☢☢☢☢☢☢☢☢☢☢☢☢☢☢☢☢☢☢☢
mer. ☢☢☢☢☢☢☢☢✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓
jeu. ✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓
ven. ✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓✓
sam. ✓✓✓✓✓✓✓✓__________________
dim. __________________________
```


### `usage: termitime [-h] [--raw] [-A] [-c COLOR_MAP] [-d] [-m MARKS] [-r VISIBLE_RANGE] [-s] [-w WIDTH] [-W [WIDE]] timetables [timetables ...]`

```
Text-based generator of timetable/timelines suitable to build activity, custody or parenting worksheets & schedules.

positional arguments:
  timetables            One or more timetables. See format below.

optional arguments:
  -h, --help            show this help message and exit
  --raw                 Dump raw calendar to stdout (uncompressed). Imply -d.
  -A, --abbrev          Use abbreaviated day names.
  -c COLOR_MAP, --color-map COLOR_MAP
                        Provide a color map.
  -d, --dump            Dump generated calendar to stdout.
  -m MARKS, --marks MARKS
                        Additional hours marks to show.
  -r VISIBLE_RANGE, --visible-range VISIBLE_RANGE
                        Visible hour range.
  -s, --stats           Width in columns.
  -w WIDTH, --width WIDTH
                        Width in columns.
  -W [WIDE], --wide [WIDE]
                        Display two week side by side.
```

### Timetable format: `<start>-<stop>[c<color>]`

Where:
* The middle dash ("-") is mandatory.
* One of `<start>` or `<stop>` may be omitted (unless it can't be deduced from a previous time-interval)
* Both `<start>` and `<stop>` follow the format: `<day>[@<hour>]`
* `<day>` can be any day in English, French or Spanish
* `<hour>` can be any common format like "12:45" or "12h15" or simply "12"

Examples:
* `lun-mer@16,-vier@16,-dim`
* `mon-thur@10c1-dim`

### About colors:
 A color-map can be specified using the `-c` | `--color-map` flag which expect a comma-separated list of
 characters (optionally using color-escape sequence).
 Eg: `-c $'▒,░'` would provide two characters, one being colored.

 Any non-trivial scenario requires at least two colors.
 The number of colors must match the number of actual hour attributions.
